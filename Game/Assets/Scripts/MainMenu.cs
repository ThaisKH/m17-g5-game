﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject loadGamebutton;
    private void Start()
    {
        if (SaveLoadManager.SaveExists())
        {
            loadGamebutton.SetActive(true);
        } else
        {
            loadGamebutton.SetActive(false);
        }
    }
    public void newGame()
    {
        if (SaveLoadManager.SaveExists()) {
            SaveLoadManager.DeleteSave();
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        GameManager._instance.setGameState(GameStates.playing);
    }

    public void loadGame()
    {
        if (SaveLoadManager.SaveExists()) {
            GameManager._instance.LoadGame();
            GameManager._instance.setGameState(GameStates.playing);
        } else {
            Debug.Log("No save data.");
        }
    }

    public void quitGame()
    {        
        Application.Quit();
        Debug.Log("QUIT!");
    }
}
