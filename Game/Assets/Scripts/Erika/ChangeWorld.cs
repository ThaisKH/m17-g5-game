﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeWorld : MonoBehaviour
{
    private Rigidbody2D _rb2D;
    private Animator _animator;
    
    public LayerMask layerCollisionGap;
    public float distance = 88f;
    public float distanceGround = 1.39f;

    bool isInPresent = true;

    void Start()
    {
        _rb2D = this.gameObject.GetComponent<Rigidbody2D>();
        _animator = this.gameObject.GetComponent<ErikaController>().GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager._instance.getGameState() == GameStates.playing) {
            if (Input.GetButtonDown("ChangeWorld")) {
                if (!isGap()) {
                    changeWorld();
                }
            }
        }
    }

    private void changeWorld()
    {
        if(isInPresent)
        {
            _rb2D.transform.position = new Vector3(_rb2D.position.x, _rb2D.position.y - distance, 0);
            isInPresent = false;
        } else
        {
            _rb2D.transform.position = new Vector3(_rb2D.position.x, _rb2D.position.y + distance - 0.1f, 0);
            isInPresent = true;
        }
        _animator.SetBool("IsInPresent", isInPresent);
        Debug.Log(isInPresent);
    }

    private bool isGap()
    {
        return Physics2D.Raycast(_rb2D.position, Vector2.down, distanceGround, layerCollisionGap);
    }
}
