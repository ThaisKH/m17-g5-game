﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public Sound[] sounds;

    public static AudioManager _instance;

    private bool fromPause = false;

    private bool notFromPlaying = true;

    private bool notFromPause = true;

    void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
        } else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }    
    }

    public void Start()
    {
      if(GameManager._instance.getGameState() == GameStates.mainMenu)
        {
            playSound("MenuTheme");
        } 
    
        
    }

    private void Update()
    {

        if (GameManager._instance.getGameState() == GameStates.playing && notFromPlaying && notFromPause)
        {
            stopSound("MenuTheme");
            playSound("MainTheme");
            notFromPlaying = false;

        }

        if (GameManager._instance.getGameState() == GameStates.playing && fromPause)
        {
            unPauseSound("MainTheme");
            fromPause = false;
        }

        if(GameManager._instance.getGameState() == GameStates.pause)
        {
            pauseSound("MainTheme");
            fromPause = true;
            notFromPlaying = true;
            notFromPause= false;
        }

      
    }


    public void playSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.soundName == name);
        s.source.Play();
    }

    public void pauseSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.soundName == name);
        s.source.Pause();
    }

    public void unPauseSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.soundName == name);
        s.source.UnPause();
    }

    public void stopSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.soundName == name);
        s.source.Stop();
    }


}
