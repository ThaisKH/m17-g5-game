﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int levelScene;
    public float[] lastCheckpointPosition;
    public List<NoteData> collectedNotes;

    public GameData(int level, Vector3 checkPoint, List<NoteData> notes)
    {
        levelScene = level;
        lastCheckpointPosition = new float[3];
        lastCheckpointPosition[0] = checkPoint.x;
        lastCheckpointPosition[1] = checkPoint.y;
        lastCheckpointPosition[2] = checkPoint.z;
        //collectedNotes = new List<NoteData>(notes);
        collectedNotes = notes;
    }
}
