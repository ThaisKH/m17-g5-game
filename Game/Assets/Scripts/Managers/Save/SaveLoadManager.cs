﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using System;
using System.Collections.Generic;

public static class SaveLoadManager {

    private const string BASE_FOLDER_NAME = "/SaveData/";
    private const string DEFAULT_FOLDER_NAME = "SaveManager";
    private const string DEFAULT_FILE_NAME = "Save";

    public static bool SaveExists(string fileName = DEFAULT_FILE_NAME, string folderName = DEFAULT_FOLDER_NAME)
    {
        string savePath = DetermineSavePath(folderName);
        string saveFileName = DetermineSaveFileName(fileName);
        if (File.Exists(savePath+saveFileName)) {
            return true;
        } else {
            return false;
        }
    }

    private static string DetermineSavePath(string folderName = DEFAULT_FOLDER_NAME)
    {
        string savePath;

        savePath = Application.dataPath + BASE_FOLDER_NAME;

        savePath = savePath + folderName + "/";
        return savePath;
    }

    private static string DetermineSaveFileName(string fileName)
    {
        return fileName + ".dat";
    }

    public static void SaveGame (int level, Vector3 checkPoint, List<NoteData> notes, string fileName = DEFAULT_FILE_NAME, string foldername = DEFAULT_FOLDER_NAME)
    {
        string savePath = DetermineSavePath(foldername);
        string saveFileName = DetermineSaveFileName(fileName);

        if (!Directory.Exists(savePath)) {
            Directory.CreateDirectory(savePath);
        }

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream saveFile = new FileStream(savePath + saveFileName, FileMode.Create);

        Debug.Log(savePath + saveFileName);

        GameData saveData = new GameData(level, checkPoint, notes);

        formatter.Serialize(saveFile, saveData);
        saveFile.Close();
    }

    public static GameData LoadGame(string fileName = DEFAULT_FILE_NAME, string foldername = DEFAULT_FOLDER_NAME)
    {
        string savePath = DetermineSavePath(foldername);
        string saveFileName = savePath + DetermineSaveFileName(fileName);

        if (!Directory.Exists(savePath) || !File.Exists(saveFileName)) {
            throw new SavedGameNotFoundException(saveFileName);
        }

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(saveFileName, FileMode.Open);

        GameData loadData = formatter.Deserialize(stream) as GameData;
        stream.Close();
        
        return loadData;
    }

    public static void DeleteSave(string fileName = DEFAULT_FILE_NAME, string folderName = DEFAULT_FOLDER_NAME)
    {
        string savePath = DetermineSavePath(folderName);
        string saveFileName = DetermineSaveFileName(fileName);
        File.Delete(savePath + saveFileName);
    }

    public static void DeleteSaveFolder(string folderName = DEFAULT_FOLDER_NAME)
    {
        string savePath = DetermineSavePath(folderName);
        Directory.Delete(savePath, true);
    }
}

public class SavedGameNotFoundException : Exception
{
    public SavedGameNotFoundException(string path) : base(path)
    {

    }
}
