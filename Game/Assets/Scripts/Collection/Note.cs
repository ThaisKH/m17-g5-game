﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Note : MonoBehaviour
{
    NoteData data;

    [Header("Note Information")]
    public int year;
    public string text;

    [Header("Note Visual Components")]
    public GameObject note;
    public Text yearText;
    public Text textText;

    private bool collected = false;

    void Start()
    {        
        data = new NoteData();
        data.year = year;
        data.text = text;
        data.note = note.name;
        data.yearText = yearText.name;
        data.textText = textText.name;
        data.collected = collected;
        foreach (NoteData n in GameManager._instance.collectedNotes) {
            if (n.year == year) {
                this.gameObject.SetActive(false);
                note.SetActive(true);
                yearText.text = year.ToString();
                textText.text = text;
            }
        }
    }

    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) {
            noteCollected();
        }
    }

    private void noteCollected()
    {
        this.gameObject.SetActive(false);
        collected = true;
        GameManager._instance.collectedNotes.Add(data);
        //GameManager._instance.collectedNotes.Sort(new YearComparer());
        note.SetActive(true);
        yearText.text = year.ToString();
        textText.text = text;
    }

}

[System.Serializable]
public class NoteData
{
    public int year;
    public string text;
    public string note;
    public string yearText;
    public string textText;
    public bool collected;
}
